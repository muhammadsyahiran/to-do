import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FormToDo extends StatefulWidget {
  const FormToDo({super.key});

  @override
  State<FormToDo> createState() => _FormToDoState();
}

class _FormToDoState extends State<FormToDo> {
  TextEditingController dateControllerStart = TextEditingController();
  TextEditingController dateControllerEnd = TextEditingController();  
  final TextEditingController _textFieldController = TextEditingController();
  
  final List<String> _todoList = <String>[];

  void _addTodoItem(String title) {
    // Wrapping it inside a set state will notify
    // the app that the state has changed
    setState(() {
      _todoList.add(title);
    });
    _textFieldController.clear();
  }
  

  @override
  void initState() {
    super.initState();
    dateControllerStart.text = "";
    dateControllerEnd.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_rounded),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('Add new To-Do List'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.fromLTRB(20, 30, 20, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              // padding: const EdgeInsets.fromLTRB(20, 30, 20, 0),
              children: [
                const Text("To-Do Title"),
                const SizedBox(height: 10),
                Container(
                  padding: const EdgeInsets.all(20),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black)),
                  child: TextFormField(
                    minLines: 5,
                    maxLines: 5,
              controller: _textFieldController,
                    decoration: const InputDecoration(
                      hintText: "Please key in your To-Do title here",
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                const Text("Start Date"),
                const SizedBox(height: 5),
                Container(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black)),
                  child: TextFormField(
                    controller: dateControllerStart,
                    decoration: const InputDecoration(hintText: "Select Date"),
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2000),
                          lastDate: DateTime(2101));
                      if (pickedDate != null) {
                        String formattedDateStart =
                            DateFormat("yyyy-MM-dd").format(pickedDate);
                        setState(() {
                          dateControllerStart.text =
                              formattedDateStart.toString();
                        });
                      }
                    },
                  ),
                ),
                const SizedBox(height: 20),
                const Text("Estimate End Date"),
                const SizedBox(height: 5),
                Container(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black)),
                  child: TextFormField(
                    controller: dateControllerEnd,
                    decoration: const InputDecoration(hintText: "Select Date"),
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2000),
                          lastDate: DateTime(2101));
                      if (pickedDate != null) {
                        String formattedDateEnd =
                            DateFormat("yyyy-MM-dd").format(pickedDate);
                        setState(() {
                          dateControllerEnd.text = formattedDateEnd.toString();
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () => {Navigator.pop(context),
                  _addTodoItem(_textFieldController.text)},
            child: Container(
              height: 55,
              color: Colors.black,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text('Create Now', style: TextStyle(color: Colors.white)),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
