import 'package:flutter/material.dart';

import '../component/cardcustom.dart';
import 'formtodo.dart';

class Home extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('To-Do List'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const FormToDo(),
              ),
            );
          },
          child: const Icon(Icons.add),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body:ListView(
            padding: const EdgeInsets.fromLTRB(10, 15, 10, 0),
            children: const <Widget>[
              Cardtodo( title:'')
            ]));
  }
}
