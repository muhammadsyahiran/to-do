import 'package:flutter/material.dart';
import 'package:to_do_list/screen/home.dart';

void main() {
  runApp(const Theme());
}

class Theme extends StatefulWidget {
  const Theme({super.key});

  @override
  State<Theme> createState() => _ThemeState();
}

class _ThemeState extends State<Theme> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
        backgroundColor: Color(0xFFFFC400),
        iconTheme: IconThemeData(color: Colors.black, size: 15),
        titleTextStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
      ),
      floatingActionButtonTheme:  const FloatingActionButtonThemeData(
        backgroundColor: Colors.orange,
        foregroundColor: Colors.white,
      ),
      ),
      home: Home(),
    );
  }
}
